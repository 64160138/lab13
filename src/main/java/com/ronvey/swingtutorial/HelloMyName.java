package com.ronvey.swingtutorial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class HelloMyName extends JFrame {
    JLabel lblName,lblHello;
    JTextField txtName;
    JButton btnHello;
    public HelloMyName(){
        super("Hello My Name");
        lblName = new JLabel("Name : ");
        lblName.setBounds(10,10,50,20);
        lblName.setHorizontalAlignment(JLabel.RIGHT);
        txtName = new JTextField();
        txtName.setBounds(70, 10, 200, 20);
        btnHello = new JButton("Hello");
        btnHello.setBounds(10,40,250,20);
        btnHello.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                String myName = txtName.getText();
                lblHello.setText("Hello "+ myName);
            }
            
        });

        lblHello = new JLabel();
        lblHello.setBounds(30,70,250,20);
        lblHello.setHorizontalAlignment(JLabel.CENTER);
        this.add(lblHello);
        this.add(btnHello);
        this.add(txtName);
        this.add(lblName);
        this.setLayout(null);
        this.setSize(400,300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
    public static void main(String[] args) {
        HelloMyName frame = new HelloMyName();
    }
}
