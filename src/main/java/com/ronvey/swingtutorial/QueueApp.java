package com.ronvey.swingtutorial;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.crypto.interfaces.PBEKey;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class QueueApp extends JFrame {
    JTextField txtName;
    JLabel lblQueueList, lblCurrent;
    JButton btnAddQueue, btnGetQueue, btnClearQueue;
    LinkedList<String> queue;

    public QueueApp() {
        super("Queue App");
        queue = new LinkedList();
        this.setSize(400, 300);
        txtName = new JTextField();
        txtName.setBounds(30, 10, 200, 20);
        txtName.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                addQueue();
            }
            
        });

        btnAddQueue = new JButton("Add Queue");
        btnAddQueue.setBounds(250, 10, 120, 20);
        btnAddQueue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                addQueue();
            }

        });
        btnGetQueue = new JButton("Get Queue");
        btnGetQueue.setBounds(250, 40, 120, 20);
        btnGetQueue.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                getQueue();
            }

        });
        btnClearQueue = new JButton("Clear Queue");
        btnClearQueue.setBounds(250, 70, 120, 20);
        btnClearQueue.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
                clearQueue();     
            }

        });

        lblQueueList = new JLabel("Empty");
        lblQueueList.setBounds(30, 40, 200, 20);
        lblCurrent = new JLabel("?");
        lblCurrent.setHorizontalAlignment(JLabel.CENTER);
        lblCurrent.setFont(new Font("Serif", Font.PLAIN, 50));
        lblCurrent.setBounds(30, 70, 200, 50);
        this.add(lblCurrent);
        this.add(btnAddQueue);
        this.add(btnGetQueue);
        this.add(btnClearQueue);
        this.add(lblQueueList);
        this.add(txtName);
        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        shouQueue();
        this.setVisible(true);
    }
    public void clearQueue(){
        queue.clear();
        lblCurrent.setText("?");
        shouQueue();
    }
    public void getQueue(){
        if(queue.isEmpty()){
            lblCurrent.setText("?");
            return;
        }
        String name = queue.remove();
        lblCurrent.setText(name);
    }

    public void shouQueue() {
        if (queue.isEmpty()) {
            lblQueueList.setText(queue.toString());
        } else {
            lblQueueList.setText(queue.toString());
        }
    }

    public void addQueue() {
        String name = txtName.getText();
        if (name.equals("")) {
            return;
        }

        queue.add(name);
        txtName.setText("");
        shouQueue();
    }
    public static void main(String[] args) {
        QueueApp queue1 = new QueueApp();
    }
}
